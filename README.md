# GPS2Rviz

在ROS2环境下转换惯导接收的经纬度坐标至本地笛卡尔坐标系，采用半正矢计算方式，起始点坐标为(0,0)点。

Convert the latitude and longitude coordinates received by the inertial navigation to the local Cartesian coordinate system in the ROS2 environment, using the half-sine calculation method, and the starting point coordinates are (0,0) point.

# Dependencies
该工具依赖于ROS2的rclcpp和一些库，需要安装ROS2环境。
依赖于自定义的消息类型Ins，需要根据自己的实际情况修改消息类型。

This tool depends on rclcpp of ROS2 and some libraries, and requires the installation of the ROS2 environment.
Depends on the custom message type Ins, you need to modify the message type according to your actual situation.
# How to build

```bash
colcon build
```
# How to use

```bash
ros2 run gps2rviz gps2rviz
```