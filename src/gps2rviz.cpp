#include <rclcpp/rclcpp.hpp>
#include <geometry_msgs/msg/pose_stamped.hpp>
#include <nav_msgs/msg/path.hpp>
#include <math.h>
#include "ins_msgs/msg/ins.hpp"

using Ins = ins_msgs::msg::Ins;
using Path = nav_msgs::msg::Path;
class Gps2rviz : public rclcpp::Node
{
    public:
    std::unique_ptr<Path> pathmsg;
    Gps2rviz() : Node("GPS2RVIZ")
    {
        init = false;
        state_pub_ = this->create_publisher<Path>("/gps2rviz/path",1);
        ins_sub_ = this->create_subscription<Ins>("/chcnav/ins",10, std::bind(&Gps2rviz::gpsCallback,this, std::placeholders::_1));
        pathmsg = std::make_unique<Path>();
        pathmsg->header.frame_id = "path";
        pathmsg->header.stamp = this->now();
    }
    struct my_pose
    {
        double latitude;
        double longitude;
        double altitude;
    };

    // 角度制转弧度制
    double rad(double d) 
    {
        return d * 3.1415926 / 180.0;
    }
    
    private:
    rclcpp::Publisher<Path>::SharedPtr state_pub_;
    rclcpp::Subscription<Ins>::SharedPtr ins_sub_;
    bool init;
    my_pose init_pose;  
    const double EARTH_RADIUS = 6378.137;//地球半径
    
    void gpsCallback(const Ins::SharedPtr ins_msg_ptr)
{
    //初始化
    if(!init)
    {
        init_pose.latitude = ins_msg_ptr->latitude;
        init_pose.longitude = ins_msg_ptr->longitude;
        init_pose.altitude = ins_msg_ptr->altitude;
        init = true;
    }
    else
    {
    //计算相对位置
        double radLat1 ,radLat2, radLong1,radLong2,delta_lat,delta_long,x,y;
		radLat1 = rad(init_pose.latitude);
        radLong1 = rad(init_pose.longitude);
		radLat2 = rad(ins_msg_ptr->latitude);
		radLong2 = rad(ins_msg_ptr->longitude);
        
        //计算x
        delta_long = 0;
	    delta_lat = radLat2 - radLat1;  //(radLat1,radLong1)-(radLat2,radLong1)
        if(delta_lat>0)
            x = 2*asin( sqrt( pow( sin( delta_lat/2 ),2) + cos( radLat1 )*cos( radLat2)*pow( sin( delta_long/2 ),2 ) ));
        else
            x=-2*asin( sqrt( pow( sin( delta_lat/2 ),2) + cos( radLat1 )*cos( radLat2)*pow( sin( delta_long/2 ),2 ) ));
        x = x*EARTH_RADIUS*1000;

        //计算y
        delta_lat = 0;
        delta_long = radLong2  - radLong1;   //(radLat1,radLong1)-(radLat1,radLong2)
        if(delta_long>0)
            y = 2*asin( sqrt( pow( sin( delta_lat/2 ),2) + cos( radLat2 )*cos( radLat2)*pow( sin( delta_long/2 ),2 ) ) );
        else
            y=-2*asin( sqrt( pow( sin( delta_lat/2 ),2) + cos( radLat2 )*cos( radLat2)*pow( sin( delta_long/2 ),2 ) ) );
            //double y = 2*asin( sin( delta_lat/2 ) + cos( radLat2 )*cos( radLat2)* sin( delta_long/2 )   );
        y = y*EARTH_RADIUS*1000;

        //计算z
        double z = ins_msg_ptr->altitude - init_pose.altitude;

        //发布轨迹
        // pathmsg->header.frame_id = "path";
        // pathmsg->header.stamp = this->now();

        geometry_msgs::msg::PoseStamped posexyz;
        // posexyz.header = pathmsg->header;
        posexyz.header.frame_id = "path";
        posexyz.header.stamp = this->now();
        posexyz.pose.position.x = x;
        posexyz.pose.position.y = y;
        posexyz.pose.position.z = 0.0;
        pathmsg->poses.push_back(posexyz);

        RCLCPP_INFO(this->get_logger(),"( x:%0.6f ,y:%0.6f ,z:%0.6f)",x ,y ,z );
        state_pub_->publish(*pathmsg);
    }
}
};

int main(int argc,char **argv)
{
    rclcpp::init(argc,argv);
    auto gps2rviz_node = std::make_shared<Gps2rviz>();
    while(rclcpp::ok)
    {
        rclcpp::spin(gps2rviz_node);
    }
    rclcpp::shutdown();
    return 0;
}
